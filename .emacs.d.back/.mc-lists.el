;; This file is automatically generated by the multiple-cursors extension.
;; It keeps track of your preferences for running commands with multiple cursors.

(setq mc/cmds-to-run-for-all
      '(
        electric-pair-delete-pair
        elpy-nav-backward-indent
        elpy-nav-forward-indent
        helm-M-x
        indent-for-tab-command
        js2r-kill
        magit-status
        org-cycle
        org-self-insert-command
        query-replace
        rjsx-electric-gt
        rjsx-electric-lt
        ))

(setq mc/cmds-to-run-once
      '(
        helm-find-files
        kill-region
        ))
