#  Dotfiles
![2025-01-07-185327](/uploads/f422a18768ccf21958dba0c2ef6cab5b/2025-01-07-185327.png)
![2025-01-07-185523](/uploads/993e7b45fe20bd01b751013f6845355a/2025-01-07-185523.png)
![2025-01-07-190422](/uploads/13c70f7b3737397348bba6318145787e/2025-01-07-190422.png)

Dotfiles are the customization files that are used to personalize your Linux or other Unix-based system.  You can tell that a file is a dotfile because the name of the file will begin with a period--a dot!  The period at the beginning of a filename or directory name indicates that it is a hidden file or directory.  This repository contains my personal dotfiles.  They are stored here for convenience so that I may quickly access them on new machines or new installs.  Also, others may find some of my configurations helpful in customizing their own dotfiles.  

# Who Am I?
<img align="left" border="0" padding="4" src="https://gitlab.com/rogs/dotfiles/-/raw/master/.me-small.png">

* My website: https://rogs.me
* My wiki: https://wiki.rogs.me
* My gitlab: https://gitlab.com/rogs

# How To Manage Your Own Dotfiles

There are a hundred ways to manage your dotfiles.  My first suggestion would be to read up on the subject.  A great place to start is "Your unofficial guide to dotfiles on GitHub": https://dotfiles.github.io/

Personally, I use the git bare repository method for managing my dotfiles: https://developer.atlassian.com/blog/2016/02/best-way-to-store-dotfiles-git-bare-repo/

# License

The files and scripts in this repository are licensed under the MIT License, which is a very permissive license allowing you to use, modify, copy, distribute, sell, give away, etc. the software.  In other words, do what you want with it.  The  only requirement with the MIT License is that the license and copyright notice must be provided with the software.
